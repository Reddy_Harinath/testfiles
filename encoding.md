2018-11-05
==========

 * Setting default encoding to UTF-8 (Miroslav Bajtoš)

 * Fixing the issue MARKDOWN-26 (Miroslav Bajtoš)

 * Test the encoding text (Miroslav Bajtoš)